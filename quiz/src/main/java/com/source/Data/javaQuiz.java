package com.source.Data;

import com.source.controller.FromNavigationApp;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;

import javafx.scene.Group;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class javaQuiz {

      private FromNavigationApp app;
     private GridPane view;
   
     
    public javaQuiz(FromNavigationApp app) {

        this.app = app;
        initialize();
      }
    
      private void initialize() {

        
        view = new GridPane();
        view.setPadding(new Insets(1,1,1,1));

        Image imgbc = new Image("aabb.png");
        ImageView ivBc = new ImageView(imgbc);
        ivBc.setFitHeight(1080);
        ivBc.setFitWidth(1920);
        ivBc.setPreserveRatio(true);
        ivBc.setOpacity(0.9);

         Text text2 = new Text("Quizzo!");
        //text2.setFont(new Font(170)); // Set the font size
        text2.getStyleClass().add("text-style"); // Set the tex
        // Position the text at the desired location
        text2.setX(150); // X position
        text2.setY(60); // Y position   

        Button button1 = new Button("<- Back");
        button1.setLayoutX(10);
        button1.setLayoutY(10);
        button1.setMinWidth(30);
        button1.setMinHeight(10);
        button1.setFont(new Font(25));;
       // button1.setStyle("-fx-border-radius:50;"+" -fx-background-radius: 50;"+"-fx-border-Color:purple;"+"-fx-background-color:purple;"+"-fx-text-fill: white;");
       button1.getStyleClass().add("button-unique-back");
       button1.setOnAction(new EventHandler<ActionEvent>() {
        
            @Override
            public void handle(ActionEvent event){

               app.navigateToPage2();
            }
       });

         
       Button bt2 = new Button("About");
       bt2.setMaxSize(130, 50);
      // bt.setStyle("-fx-font-size: 16px; -fx-background-color: #ff7f50; -fx-text-fill: white;");
       bt2.setStyle("-fx-font-size: 25px; -fx-background-color:#87CEEB; -fx-text-fill: black; -fx-border-radius: 5px; -fx-background-radius: 5px; -fx-border-color: #315aff;");
       bt2.setOnMousePressed(e -> bt2.setStyle("-fx-font-size: 25px; -fx-background-color: linear-gradient(#A9A9A9); -fx-text-fill: red; -fx-border-radius: 5px; -fx-background-radius: 5px;"));
        bt2.setLayoutX(1650);
        bt2.setLayoutY(30);
       bt2.setOnAction(new EventHandler<ActionEvent>() {
        
            @Override
            public void handle(ActionEvent event){

          app.navigateToPage7();
            }
       });


        Button bt3 = new Button("Contact");
       bt3.setMaxSize(130, 50);
      // bt.setStyle("-fx-font-size: 16px; -fx-background-color: #ff7f50; -fx-text-fill: white;");
       bt3.setStyle("-fx-font-size: 25px; -fx-background-color:#87CEEB; -fx-text-fill: black; -fx-border-radius: 5px; -fx-background-radius: 5px; -fx-border-color: #315aff;");
       bt3.setOnMousePressed(e -> bt2.setStyle("-fx-font-size: 25px; -fx-background-color: linear-gradient(#A9A9A9); -fx-text-fill: red; -fx-border-radius: 5px; -fx-background-radius: 5px;"));
        bt3.setLayoutX(1790);
        bt3.setLayoutY(30);
        bt3.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event){

             app.navigateToPage8();
            }
        });

        // Label QQ3 = new Label("Welcome to Java Quiz");
        // QQ3.setStyle("-fx-font-size: 35px;");
        // // QQ3.setScaleX(2);
        // // QQ3.setScaleY(2);
        // // Button bt2 = new Button("Start");
        // // bt2.getStyleClass().add("bt-st");
        // // Label lb2 = new Label("Java Quiz");
        // // lb2.getStyleClass().add("lable");
        // Image img2 = new Image("Java.jpeg");
        // ImageView iv2 = new ImageView(img2);
        // iv2.setFitWidth(150);
        // iv2.setFitHeight(150);
        // VBox quiz2 = new VBox(iv2,QQ3);
        // quiz2.setMinWidth(500);
        // quiz2.setMinHeight(400);
        // quiz2.setStyle("-fx-background-radius:250");
        // quiz2.setLayoutX(730);
        // quiz2.setLayoutY(0);
        // quiz2.getStyleClass().add("quiz-style");
       
        //quiz2.setMargin(lb2, new Insets(27, 0, 0, 30));  
        //quiz2.setMargin(QQ3, new Insets(130, 0, 0, 70));
        //quiz2.setMargin(bt2, new Insets(40, 0, 0, 50));
        //quiz2.setAlignment(Pos.BOTTOM_CENTER);
        //quiz2.setMargin(iv2, new Insets(20, 0, 0, 250));
        Label lb1 = new Label("Control Statement");
        lb1.setFont(new Font(30));
        lb1.getStyleClass().add("hb-topic");

        Label lb2 = new Label("Input & Output");
        lb2.setFont(new Font(30));
        lb2.getStyleClass().add("hb-topic");

        Label lb3 = new Label("Array");
        lb3.setFont(new Font(30));
        lb3.getStyleClass().add("hb-topic");

        Label lb4 = new Label("String");
        lb4.setFont(new Font(30));
        lb4.getStyleClass().add("hb-topic");

        VBox vb = new VBox(lb1,lb2,lb3,lb4);
        vb.setMinHeight(700);
        vb.setMinWidth(450);
        vb.setStyle("-fx-background-Color:Red;"+"-fx-background-radius:30;");
        vb.setLayoutX(80);
        vb.setLayoutY(200);
        vb.setMargin(lb1, new Insets(50, 0, 0, 40));
        vb.setMargin(lb2, new Insets(90, 0, 0, 40));
        vb.setMargin(lb3, new Insets(110, 0, 0, 40));
        vb.setMargin(lb4, new Insets(120, 0, 0, 40));

        Button cons = new Button("Start");
        cons.getStyleClass().add("bt-st");
        cons.setOnAction(event -> showPopup());

        Button IaO = new Button("Start");
        IaO.getStyleClass().add("bt-st");

        Button arr = new Button("Start");
        arr.getStyleClass().add("bt-st");

        Button str = new Button("Start");
        str.getStyleClass().add("bt-st");


        VBox vb1 = new VBox(cons,IaO,arr,str);
        vb1.setMinHeight(700);
        vb1.setMinWidth(200);
        vb1.setStyle("-fx-background-Color:Gray;"+"-fx-background-radius:30;");
        vb1.setLayoutX(580);
        vb1.setLayoutY(200);
        vb1.setMargin(cons, new Insets(50, 0, 0, 40));
        vb1.setMargin(IaO, new Insets(90, 0, 0, 40));
        vb1.setMargin(arr, new Insets(120, 0, 0, 40));
        vb1.setMargin(str, new Insets(140, 0, 0, 40));


        Label lb5 = new Label("Classes & Objects");
        lb5.setFont(new Font(30));
        lb5.getStyleClass().add("hb-topic");

        Label lb6 = new Label("Inheritance");
        lb6.setFont(new Font(30));
        lb6.getStyleClass().add("hb-topic");

        Label lb7 = new Label("Polymorphism");
        lb7.setFont(new Font(30));
        lb7.getStyleClass().add("hb-topic");

        Label lb8 = new Label("Abstract Classes");
        lb8.setFont(new Font(30));
        lb8.getStyleClass().add("hb-topic");

        VBox vb2 = new VBox(lb5,lb6,lb7,lb8);
        vb2.setMinHeight(700);
        vb2.setMinWidth(450);
        vb2.setStyle("-fx-background-Color:red;"+"-fx-background-radius:30;");
        vb2.setLayoutX(1130);
        vb2.setLayoutY(200);
        vb2.setMargin(lb5, new Insets(50, 0, 0, 40));
        vb2.setMargin(lb6, new Insets(90, 0, 0, 40));
        vb2.setMargin(lb7, new Insets(110, 0, 0, 40));
        vb2.setMargin(lb8, new Insets(120, 0, 0, 40));


        Button cao = new Button("Start");
        cao.getStyleClass().add("bt-st");

        Button inh = new Button("Start");
        inh.getStyleClass().add("bt-st");

        Button poly = new Button("Start");
        poly.getStyleClass().add("bt-st");

        Button abc = new Button("Start");
        abc.getStyleClass().add("bt-st");

        VBox vb3 = new VBox(cao,inh,poly,abc);
        vb3.setMinHeight(700);
        vb3.setMinWidth(200);
        vb3.setStyle("-fx-background-Color:Gray;"+"-fx-background-radius:30;");
        vb3.setLayoutX(1630);
        vb3.setLayoutY(200);
        vb3.setMargin(cao, new Insets(50, 0, 0, 40));
        vb3.setMargin(inh, new Insets(90, 0, 0, 40));
        vb3.setMargin(poly, new Insets(120, 0, 0, 40));
        vb3.setMargin(abc, new Insets(140, 0, 0, 40));

        Image image = new Image("homeButton.png"); // Make sure the path is correct

        // Create an ImageView
        ImageView imageView = new ImageView(image);

        // Optionally set the size of the ImageView
        imageView.setFitWidth(56);
        imageView.setFitHeight(56);

        Button button2 = new Button();
        button2.setGraphic(imageView);
        button2.setStyle("-fx-background-color:#CBC3E3;"+"-fx-text-fill: white;");
        button2.setOnMouseEntered(e -> button2.setStyle("-fx-background-color: red;"));
        button2.setOnMouseExited(e -> button2.setStyle("-fx-background-color: pink;"));
        button2.setOnAction(new EventHandler<ActionEvent>() {
          @Override
          public void handle(ActionEvent event){
            app.navigateToPage3();
          }
        });


        Image image3 = new Image("profile.jpg"); // Make sure the path is correct

        // Create an ImageView
        ImageView imageView3 = new ImageView(image3);

        // Optionally set the size of the ImageView
        imageView3.setFitWidth(56);
        imageView3.setFitHeight(56);

        // Create a Button and set the ImageView as its graphic
        Button button4 = new Button();
        button4.setGraphic(imageView3);
        button4.setStyle("-fx-background-color:#CBC3E3;"+"-fx-text-fill: white;");
        button4.setOnMouseEntered(e -> button4.setStyle("-fx-background-color: red;"));
        button4.setOnMouseExited(e -> button4.setStyle("-fx-background-color: pink;"));
        button4.setOnAction(new EventHandler<ActionEvent>() {
          @Override
          public void handle(ActionEvent event){
            app.navigateToPage6();
          }
        });


        HBox hBox1 = new HBox(button2,button4);
        hBox1.setMinWidth(1600);
        hBox1.setMinHeight(60); 
        
        hBox1.setLayoutY(910);
        hBox1.setLayoutX(110);
        hBox1.setMargin(button2, new Insets(10, 0, 0, 180)); 
        hBox1.setMargin(button4, new Insets(10, 0, 0, 1100)); 
        hBox1.getStyleClass().add("hbox-footer");

        StackPane stp = new StackPane(ivBc);

        Group gp = new Group(stp,text2,button1,vb,vb1,vb2,vb3,bt2,bt3,hBox1);

        view.add(gp,0,0);
        //Scene  scene = new Scene(gp,1920,1080);
       // scene.getStylesheets().add(getClass().getResource("style3.css").toExternalForm());
        // primaryStage.setScene(scene);
        // //scene.setFill(Color.SKYBLUE);
        // primaryStage.show();
    }

     // Method to create and show the popup window
    private void showPopup() {
        // Create a new stage for the popup
        Stage popupStage = new Stage();

        Label lb1 = new Label("Quiz No :- 1 ");
        lb1.setFont(new Font(30));
        Button bt1 = new Button("Start");
        bt1.getStyleClass().add("bt-st");
        bt1.setOnAction(new EventHandler<ActionEvent>() {
          @Override
          public void handle(ActionEvent event){
            app.navigateToPage9();
          }
        });
      //  bt1.setStyle("-fx-font-size: 20px; -fx-background-color:#87CEEB; -fx-text-fill: black; -fx-border-radius: 5px; -fx-background-radius: 5px; -fx-border-color: #315aff;");
        HBox hb1 =  new HBox(lb1,bt1);
        hb1.setMinHeight(80);
        hb1.setMinWidth(400);
        hb1.setStyle("-fx-background-Color:Red;"+"-fx-background-radius:30;");
        hb1.setLayoutX(50);
        hb1.setLayoutY(100);
        hb1.setMargin(lb1, new Insets(15, 0, 0, 30)); 
        hb1.setMargin(bt1, new Insets(15, 0, 0, 70)); 

        Label lb2 = new Label("Quiz No :- 2 ");
        lb2.setFont(new Font(30));
        Button bt2 = new Button("Start");
        bt2.getStyleClass().add("bt-st");
        //bt2.setStyle("-fx-font-size: 20px; -fx-background-color:#87CEEB; -fx-text-fill: black; -fx-border-radius: 5px; -fx-background-radius: 5px; -fx-border-color: #315aff;");
        HBox hb2 =  new HBox(lb2,bt2);
        hb2.setMinHeight(80);
        hb2.setMinWidth(400);
        hb2.setStyle("-fx-background-Color:Red;"+"-fx-background-radius:30;");
        hb2.setLayoutX(50);
        hb2.setLayoutY(200);
        hb2.setMargin(lb2, new Insets(15, 0, 0, 30)); 
        hb2.setMargin(bt2, new Insets(15, 0, 0, 70)); 

        Label lb3 = new Label("Quiz No :- 3 ");
        lb3.setFont(new Font(30));
        Button bt3 = new Button("Start");
        bt3.getStyleClass().add("bt-st");
       // bt3.setStyle("-fx-font-size: 20px; -fx-background-color:#87CEEB; -fx-text-fill: black; -fx-border-radius: 5px; -fx-background-radius: 5px; -fx-border-color: #315aff;");
        HBox hb3 =  new HBox(lb3,bt3);
        hb3.setMinHeight(80);
        hb3.setMinWidth(400);
        hb3.setStyle("-fx-background-Color:Red;"+"-fx-background-radius:30;");
        hb3.setLayoutX(50);
        hb3.setLayoutY(300);
        hb3.setMargin(lb3, new Insets(15, 0, 0, 30)); 
        hb3.setMargin(bt3, new Insets(15, 0, 0, 70)); 

        Label lb4 = new Label("Quiz No :- 4 ");
        lb4.setFont(new Font(30));
        Button bt4 = new Button("Start");
        bt4.getStyleClass().add("bt-st");
       // bt4.setStyle("-fx-font-size: 20px; -fx-background-color:#87CEEB; -fx-text-fill: black; -fx-border-radius: 5px; -fx-background-radius: 5px; -fx-border-color: #315aff;");
        HBox hb4 =  new HBox(lb4,bt4);
        hb4.setMinHeight(80);
        hb4.setMinWidth(400);
        hb4.setStyle("-fx-background-Color:Red;"+"-fx-background-radius:30;");
        hb4.setLayoutX(50);
        hb4.setLayoutY(400);
        hb4.setMargin(lb4, new Insets(15, 0, 0, 30)); 
        hb4.setMargin(bt4, new Insets(15, 0, 0, 70)); 

        Label lb5 = new Label("Quiz No :- 5 ");
        lb5.setFont(new Font(30));
        Button bt5 = new Button("Start");
        bt5.getStyleClass().add("bt-st");
       // bt5.setStyle("-fx-font-size: 20px; -fx-background-color:#87CEEB; -fx-text-fill: black; -fx-border-radius: 5px; -fx-background-radius: 5px; -fx-border-color: #315aff;");
        HBox hb5 =  new HBox(lb5,bt5);
        hb5.setMinHeight(80);
        hb5.setMinWidth(400);
        hb5.setStyle("-fx-background-Color:Red;"+"-fx-background-radius:30;");
        hb5.setLayoutX(50);
        hb5.setLayoutY(500);
        hb5.setMargin(lb5, new Insets(15, 0, 0, 30)); 
        hb5.setMargin(bt5, new Insets(15, 0, 0, 70)); 

        Label lb6 = new Label("Quiz No :- 6 ");
        lb6.setFont(new Font(30));
        Button bt6 = new Button("Start");
        bt6.getStyleClass().add("bt-st");
       // bt5.setStyle("-fx-font-size: 20px; -fx-background-color:#87CEEB; -fx-text-fill: black; -fx-border-radius: 5px; -fx-background-radius: 5px; -fx-border-color: #315aff;");
        HBox hb6 =  new HBox(lb6,bt6);
        hb6.setMinHeight(80);
        hb6.setMinWidth(400);
        hb6.setStyle("-fx-background-Color:Red;"+"-fx-background-radius:30;");
        hb6.setLayoutX(50);
        hb6.setLayoutY(600);
        hb6.setMargin(lb6, new Insets(15, 0, 0, 30)); 
        hb6.setMargin(bt6, new Insets(15, 0, 0, 70)); 

        Label lb7 = new Label("Quiz No :- 7 ");
        lb7.setFont(new Font(30));
        Button bt7 = new Button("Start");
        bt7.getStyleClass().add("bt-st");
       // bt5.setStyle("-fx-font-size: 20px; -fx-background-color:#87CEEB; -fx-text-fill: black; -fx-border-radius: 5px; -fx-background-radius: 5px; -fx-border-color: #315aff;");
        HBox hb7 =  new HBox(lb7,bt7);
        hb7.setMinHeight(80);
        hb7.setMinWidth(400);
        hb7.setStyle("-fx-background-Color:Red;"+"-fx-background-radius:30;");
        hb7.setLayoutX(50);
        hb7.setLayoutY(700);
        hb7.setMargin(lb7, new Insets(15, 0, 0, 30)); 
        hb7.setMargin(bt7, new Insets(15, 0, 0, 70)); 

        Label lb8 = new Label("Quiz No :- 8 ");
        lb8.setFont(new Font(30));
        Button bt8 = new Button("Start");
        bt8.getStyleClass().add("bt-st");
       // bt5.setStyle("-fx-font-size: 20px; -fx-background-color:#87CEEB; -fx-text-fill: black; -fx-border-radius: 5px; -fx-background-radius: 5px; -fx-border-color: #315aff;");
        HBox hb8 =  new HBox(lb8,bt8);
        hb8.setMinHeight(80);
        hb8.setMinWidth(400);
        hb8.setStyle("-fx-background-Color:Red;"+"-fx-background-radius:30;");
        hb8.setLayoutX(50);
        hb8.setLayoutY(800);
        hb8.setMargin(lb8, new Insets(15, 0, 0, 30)); 
        hb8.setMargin(bt8, new Insets(15, 0, 0, 70)); 


        // Create a button to close the popup
        Button closeButton = new Button("Close");
        closeButton.setOnAction(event -> popupStage.close());
        closeButton.setLayoutX(220);
        closeButton.setLayoutY(930);

        Group gp = new Group(hb1,hb2,hb3,hb4,hb5,hb6,hb7,hb8,closeButton);
        // Create the popup layout and add the button to it
       // StackPane popupRoot = new StackPane(gp);
        //popupRoot.setPrefSize(500, 800);

        // Create and set up the popup scene
        Scene popupScene = new Scene(gp,500,1080);
        popupStage.setTitle("Popup Window");
        popupScene.setFill(Color.BLACK);
        popupScene.getStylesheets().add(getClass().getResource("style3.css").toExternalForm());
        popupStage.setScene(popupScene);
       
    
        // Show the popup window
        popupStage.show();
    }

    public GridPane getView() {

        return view;
      }
    
      // public String getField4Value() {
    
      //   return field4.getText();
      // }
    
      // public void setField4Value(String value) {
    
      //   field4.setText(value);
      // }
}
