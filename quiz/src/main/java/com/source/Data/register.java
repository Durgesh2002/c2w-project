package com.source.Data;



import java.io.FileInputStream;
import java.io.IOException;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.source.controller.FromNavigationApp;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Glow;
import javafx.scene.effect.Reflection;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

public class register  {

    private FromNavigationApp app;
    private loginController logincontroller;
    private GridPane view;
    //private TextField field8;
    
    public register(FromNavigationApp app){

        this.app = app;
        initialize();
    }

    private void initialize(){

        view = new GridPane();
        view.setPadding(new Insets(1,1,1,1));
        //view.setStyle("-fx-background-Color:LIGHTGREEN");

        Group root = new Group();


         try {

            FileInputStream serviceAccount = new FileInputStream("src/main/resources/fx-auth-fb.json");

            FirebaseOptions options = new FirebaseOptions.Builder()
            .setCredentials(GoogleCredentials.fromStream(serviceAccount))
            .setDatabaseUrl("https://quizz-pro-default-rtdb.firebaseio.com")
            .build();

            FirebaseApp.initializeApp(options);
        }
        catch(IOException e){
            e.printStackTrace();
        }


        // Sign in Label
        Label label = createStyledLabel("Sign in", "Arial", FontWeight.BOLD, 35);
        label.setTextFill(Color.BLACK);
       
        // Email Label and Field
        Label emaiLabel = new Label("Email");
        emaiLabel.setPadding(new Insets(50, 0, 5, 0));
        emaiLabel.setStyle("-fx-text-fill:WHITE;");
        emaiLabel.setFont(Font.font("Arial", 12));
        TextField emailField = new TextField();
        emailField.setMaxWidth(250);

        // Password Label and Field
        Label passLabel = new Label("Password");
        passLabel.setPadding(new Insets(60, 0, 7, 0));
        passLabel.setStyle("-fx-text-fill:WHITE;");
        passLabel.setFont(Font.font("Arial", 12));
        PasswordField passwordField = new PasswordField();
        passwordField.setMaxWidth(250);
        logincontroller = new loginController (app,this,emailField,passwordField);
        // Error Label
        Label errorLa = new Label();
        errorLa.setStyle("-fx-text-fill:red;");
        errorLa.setPadding(new Insets(20, 0, 0, 0));

        // Login Button
        Button loginButton = createStyledButton("Login");
        loginButton.setLayoutX(380);
        loginButton.setLayoutY(580);
        
        loginButton.setOnMouseEntered(event -> loginButton.setStyle(
                "-fx-background-color: ORANGE; -fx-text-fill: WHITE; -fx-font-size: 18px; -fx-min-width: 130px; -fx-min-height: 40px;-fx-background-radius:20;"));
        loginButton.setOnMouseExited(event -> loginButton.setStyle(
                "-fx-background-color: RED; -fx-text-fill: WHITE; -fx-font-size: 18px; -fx-min-width: 130px; -fx-min-height: 40px;-fx-background-radius:20;"));
        loginButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event){
                logincontroller.login();
            }
        });


        Button signUpButton = createStyledButton("Sign Up");
        signUpButton.setLayoutX(370);
        signUpButton.setLayoutY(600);
        signUpButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event){
                    logincontroller.signUp();
                }
        });
        // Back Button
        Button backButton = createStyledButton("<-");
        backButton.setLayoutX(10);
        backButton.setLayoutY(10);
        backButton.setStyle(
                "-fx-min-width:60px;-fx-min-height:30px;-fx-background-radius:20;-fx-background-color:BLACK;-fx-text-fill:WHITE");
        backButton.setOnMouseEntered(event -> backButton.setStyle(
                "-fx-background-color:RED;-fx-min-width:60px;-fx-min-height:30px;-fx-background-radius:20;"));
        backButton.setOnMouseExited(event -> backButton.setStyle(
                "-fx-min-width:60px;-fx-min-height:30px;-fx-background-radius:20;-fx-background-color:BLACK;-fx-text-fill:WHITE"));
        backButton.setOnAction(new EventHandler<ActionEvent>() {
            
            public void handle(ActionEvent event){
                app.navigateToPage1();
            }
        });
        // Social Media Icons
        ImageView googleView = createImageView("google.png", 55, 55, 250, 650);
        ImageView facebookView = createImageView("facebook.png", 60, 60, 400, 650);
        ImageView githubView = createImageView("github.png", 75, 75, 550, 647);

        // Form Layout
        VBox formLayout = new VBox(label,  emaiLabel, emailField, passLabel, passwordField, errorLa,signUpButton);
        formLayout.setAlignment(Pos.TOP_CENTER);
        formLayout.setMinSize(500, 600);
        formLayout.setLayoutX(200);
        formLayout.setLayoutY(150);
        formLayout.setStyle("-fx-background-color:BLACK;-fx-background-radius: 20;");

        // Background Image
        ImageView backImageView = new ImageView(new Image("pinksc.jpg"));
        backImageView.setFitHeight(1080);
        backImageView.setFitWidth(1920);

        StackPane stackPane = new StackPane(backImageView);

        // Scene Setup
        Group circles = createCircles();
        Rectangle colors = createGradientRectangle();
        Group blendModeGroup = new Group(new Group(new Rectangle(1920, 1080, Color.WHITE), circles), colors);
        colors.setBlendMode(BlendMode.OVERLAY);

        root.getChildren().addAll(stackPane, blendModeGroup, formLayout, backButton, googleView, facebookView,
                githubView, loginButton);

        Text text2 = createStyledText("Quizzo!", "Arial", FontWeight.BOLD, FontPosture.ITALIC, 50, 50, 60);

        root.getChildren().add(text2);

        view.add(root,0,0);

        // Scene scene = new Scene(root, 1920, 1080);
        // scene.setFill(Color.PINK);
        // scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
        // prStage.setScene(scene);
        // prStage.setResizable(false);

        animateCircles(circles);

//         prStage.show();
     }
    
    public GridPane getView(){

        return view;
    }
  

    private Text createStyledText(String content, String fontFamily, FontWeight fontWeight, FontPosture fontPosture,
            double fontSize, double x, double y) {
        Text text = new Text(content);
        text.setFont(Font.font(fontFamily, fontWeight, fontPosture, fontSize));
        text.setFill(Color.BLACK);
        text.setX(1500);
        text.setY(950);

        // DropShadow effect
        DropShadow dropShadow = new DropShadow();
        dropShadow.setOffsetX(3.0);
        dropShadow.setOffsetY(3.0);
        dropShadow.setColor(Color.color(0.4, 0.4, 0.4));

        // Glow effect
        Glow glow = new Glow();
        glow.setLevel(0.8);

        // Reflection effect
        Reflection reflection = new Reflection();
        reflection.setFraction(0.7);

        // Applying effects
        text.setEffect(dropShadow);
        dropShadow.setInput(glow);
        glow.setInput(reflection);

        return text;
    }

    private Label createStyledLabel(String text, String fontFamily, FontWeight fontWeight, double fontSize) {
        Label label = new Label(text);
        label.setFont(Font.font(fontFamily, fontWeight, fontSize));
        label.setTextFill(Color.WHITE);
        label.setPadding(new Insets(20));
        label.setBackground(new Background(new BackgroundFill(Color.PURPLE, new CornerRadii(10), Insets.EMPTY)));
        label.setBorder(new Border(
                new BorderStroke(Color.WHITE, BorderStrokeStyle.SOLID, new CornerRadii(10), new BorderWidths(2))));
        label.setEffect(new DropShadow(10, Color.BLACK));
        return label;
    }

    private Button createStyledButton(String text) {
        Button button = new Button(text);
        button.setStyle(
                "-fx-background-color:RED; -fx-text-fill: WHITE; -fx-font-size: 18px; -fx-min-width: 130px; -fx-min-height: 40px;-fx-background-radius:20;");
        return button;
    }

    private ImageView createImageView(String imagePath, double width, double height, double layoutX, double layoutY) {
        ImageView imageView = new ImageView(new Image(imagePath));
        imageView.setFitWidth(width);
        imageView.setFitHeight(height);
        imageView.setLayoutX(layoutX);
        imageView.setLayoutY(layoutY);
        return imageView;
    }

    private Group createCircles() {
        Group circles = new Group();
        for (int i = 0; i < 30; i++) {
            Circle circle = new Circle(150, Color.web("purple", 1));
            circle.setStrokeType(StrokeType.INSIDE);
            circle.setStroke(Color.web("black", 0.02));
            circle.setStrokeWidth(6);
            circles.getChildren().add(circle);
        }
        return circles;
    }

    private Rectangle createGradientRectangle() {
        return new Rectangle(1920, 1080,
                new LinearGradient(0f, 1f, 1f, 0f, true, CycleMethod.NO_CYCLE, new Stop[] {
                        new Stop(0, Color.web("#f8bd55")),
                        new Stop(0.14, Color.web("#c0fe56")),
                        new Stop(0.28, Color.web("#5dfbc1")),
                        new Stop(0.43, Color.web("#64c2f8")),
                        new Stop(0.57, Color.web("#be4af7")),
                        new Stop(0.71, Color.web("#ed5fc2")),
                        new Stop(0.85, Color.web("#ef504c")),
                        new Stop(1, Color.web("#f2660f"))
                }));
    }

    private void animateCircles(Group circles) {
        Timeline timeline = new Timeline();
        for (javafx.scene.Node circle : circles.getChildren()) {
            timeline.getKeyFrames().addAll(
                    new KeyFrame(Duration.ZERO,
                            new KeyValue(circle.translateXProperty(), Math.random() * 800),
                            new KeyValue(circle.translateYProperty(), Math.random() * 600)),
                    new KeyFrame(new Duration(40000),
                            new KeyValue(circle.translateXProperty(), Math.random() * 800),
                            new KeyValue(circle.translateYProperty(), Math.random() * 600)));
        }
        timeline.play();
    }

}
