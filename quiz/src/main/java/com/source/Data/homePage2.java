package com.source.Data;

import com.source.controller.FromNavigationApp;

import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.ParallelTransition;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Glow;
import javafx.scene.effect.Reflection;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

public class homePage2  {

     private FromNavigationApp app;
     private GridPane view;
     private TextField field3;

     public homePage2(TextField field,GridPane view, FromNavigationApp app) {
        this.app = app;
        this.field3 = field3;
    }

    public homePage2(FromNavigationApp app) {

        this.app = app;
        initialize();
      }
      
      private final String[] imagePaths = {
        "cshome1.jpg",
        "chemhome1.jpg",
        "mathhome3.jpg",
        "phyhome1.jpg",
        "mathhome1.jpg",
        "cshome2.jpg",
        "chemhome2.jpg",
        "phyhome2.jpg",
        "mathhome2.jpg"
};

private final ImageView currentImageView = new ImageView();
private final ImageView nextImageView = new ImageView();

private int currentIndex = 0;

      private void initialize() {

        view = new GridPane();
        view.setPadding(new Insets(1,1,1,1));
        view.setStyle("-fx-background-Color:LIGHTGREEN");

         applyRoundedCorners(currentImageView, 20);
        applyRoundedCorners(nextImageView, 20);

        updateImage();

        Text textStyle = createStyledText("Quizzo!", "Arial", FontWeight.BOLD, FontPosture.ITALIC, 50, 50, 60);

        Image img1 = new Image("homeimg.jpg");

        ImageView imageView = new ImageView(img1);
        imageView.setFitWidth(70);
        imageView.setFitHeight(70);
        imageView.setPreserveRatio(false);

        Button bt1 = new Button();
        bt1.setGraphic(imageView);
        bt1.setMinHeight(5);
        bt1.setMinWidth(5);
        bt1.setOnAction(new EventHandler<ActionEvent>() {
          @Override
          public void handle(ActionEvent event){
            app.navigateToPage3();
          }
        });

        Image img2 = new Image("contanctimg.png");

        ImageView imageView1 = new ImageView(img2);
        imageView1.setFitWidth(70);
        imageView1.setFitHeight(70);
        imageView1.setPreserveRatio(false);

        Button bt2 = new Button();
        bt2.setGraphic(imageView1);
        bt2.setMinHeight(5);
        bt2.setMinWidth(5);
        bt2.setOnAction(new EventHandler<ActionEvent>() {
          @Override
          public void handle(ActionEvent event){
            app.navigateToPage8();
          }
        });

        Image img3 = new Image("addimg.png");

        ImageView imageView2 = new ImageView(img3);
        imageView2.setFitWidth(70);
        imageView2.setFitHeight(70);
        imageView2.setPreserveRatio(false);

        Button bt3 = new Button();
        bt3.setGraphic(imageView2);
        bt3.setMinHeight(0);
        bt3.setMinWidth(5);

        Image img4 = new Image("exitimg.png");

        ImageView imageView3 = new ImageView(img4);
        imageView3.setFitWidth(70);
        imageView3.setFitHeight(70);
        imageView3.setPreserveRatio(false);

        Button bt4 = new Button();
        bt4.setGraphic(imageView3);
        bt4.setMinHeight(5);
        bt4.setMinWidth(5);
        bt4.setOnAction(new EventHandler<ActionEvent>() {
          @Override
          public void handle(ActionEvent event){
            app.navigateToPage1();
          }
        });
        HBox hb1 = new HBox(bt1, bt2, bt3, bt4);
        hb1.setMinWidth(700);
        hb1.setMinHeight(100);
        hb1.setLayoutX(550);
        hb1.setLayoutY(900);
        hb1.setStyle("-fx-background-color:DARKGREEN;" + "-fx-background-radius:30;");
        hb1.setMargin(bt1, new Insets(7, 0, 0, 60));
        hb1.setMargin(bt2, new Insets(7, 0, 0, 70));
        hb1.setMargin(bt3, new Insets(7, 0, 0, 80));
        hb1.setMargin(bt4, new Insets(7, 0, 0, 60));

        Label lb2 = new Label("Programming Languages");
        lb2.setFont(new javafx.scene.text.Font(35));
        lb2.setStyle("-fx-background-color:black;-fx-text-fill:white;-fx-background-radius:20");

        Image image1 = new Image("ProgrammingLanguage.gif");
        ImageView iv = new ImageView(image1);
        iv.setFitHeight(100);
        iv.setFitWidth(100);
        iv.setStyle("-fx-background-radius:20");

        HBox hb2 = new HBox(iv, lb2);
        hb2.setMinWidth(700);
        hb2.setMinHeight(150);
        hb2.setStyle("-fx-background-radius:50;" + "-fx-background-color:BLACK;");
        hb2.setLayoutX(100);
        hb2.setLayoutY(150);
        hb2.setMargin(iv, new Insets(20, 0, 0, 20));
        hb2.setMargin(lb2, new Insets(40, 0, 0, 20));

        Label lb3 = new Label("Mathmatics");
        lb3.setFont(new javafx.scene.text.Font(35));
        lb3.setStyle("-fx-background-color:black;-fx-text-fill:white;-fx-background-radius:20");

        Image image2 = new Image("Maths.gif");
        ImageView iv2 = new ImageView(image2);
        iv2.setFitHeight(100);
        iv2.setFitWidth(100);
        iv2.setStyle("-fx-background-radius:50");

        HBox hb3 = new HBox(iv2, lb3);
        hb3.setMinWidth(700);
        hb3.setMinHeight(150);
        hb3.setStyle("-fx-background-radius:50;" + "-fx-background-color:BLACK;");
        hb3.setLayoutX(100);
        hb3.setLayoutY(340);
        hb3.setMargin(iv2, new Insets(20, 0, 0, 20));
        hb3.setMargin(lb3, new Insets(40, 0, 0, 20));

        Label lb5 = new Label("Chemistry");
        lb5.setFont(new javafx.scene.text.Font(35));
        lb5.setStyle("-fx-background-color:black;-fx-text-fill:white;-fx-background-radius:20");

        Image image5 = new Image("chemistry.gif");
        ImageView iv5 = new ImageView(image5);
        iv5.setFitHeight(100);
        iv5.setFitWidth(100);
        iv5.setStyle("-fx-background-radius:50");

        HBox hb4 = new HBox(iv5, lb5);
        hb4.setMinWidth(700);
        hb4.setMinHeight(150);
        hb4.setStyle("-fx-background-radius:50;" + "-fx-background-color:BLACK;");
        hb4.setLayoutX(100);
        hb4.setLayoutY(520);
        hb4.setMargin(iv5, new Insets(20, 0, 0, 20));
        hb4.setMargin(lb5, new Insets(40, 0, 0, 20));

        Label lb4 = new Label("Physics");
        lb4.setFont(new javafx.scene.text.Font(35));
        lb4.setStyle("-fx-background-color:black;-fx-text-fill:white;-fx-background-radius:20");

        Image image3 = new Image("physics.gif");
        ImageView iv3 = new ImageView(image3);
        iv3.setFitHeight(100);
        iv3.setFitWidth(100);

        iv3.setStyle("-fx-background-radius:10");

        HBox hb5 = new HBox(iv3, lb4);
        hb5.setMinWidth(700);
        hb5.setMinHeight(150);
        hb5.setStyle("-fx-background-radius:50;" + "-fx-background-color:BLACK;");
        hb5.setLayoutX(100);
        hb5.setLayoutY(700);
        hb5.setMargin(iv3, new Insets(20, 0, 0, 20));
        hb5.setMargin(lb4, new Insets(40, 0, 0, 20));

        Image searchimg = new Image("searchbox.jpg");
        ImageView imageViewSc = new ImageView(searchimg);
        imageViewSc.setFitWidth(40);
        imageViewSc.setFitHeight(40);

        TextField searchField = new TextField();
        searchField.setPromptText("Search...");
        searchField.setMinWidth(900);
        searchField.setMinHeight(60);
        searchField.setStyle("-fx-background-radius:50;-fx-background-color:WHITE;");

        Button searchButton = new Button();
        searchButton.setGraphic(imageViewSc);
        searchButton.setMinHeight(60);
        searchButton.getStyleClass().add("bt-st");

        searchButton.setOnAction(e -> {
            String query = searchField.getText();
            System.out.println("Searching for: " + query);
        });

        HBox searchBox = new HBox(5, searchField, searchButton);
        searchBox.setPadding(new Insets(10));
        searchBox.setLayoutX(500);

        StackPane root2 = new StackPane(currentImageView, nextImageView);
        root2.setMargin(currentImageView, new Insets(200, 0, 0, 950));
        root2.setMargin(nextImageView, new Insets(200, 0, 0, 950));
        nextImageView.setVisible(false);

        StackPane root = new StackPane();

        Button frwrdbt1 = new Button("->");
        frwrdbt1.setMinHeight(5);
        frwrdbt1.setMinWidth(5);
        frwrdbt1.setLayoutX(780);
        frwrdbt1.setLayoutY(190);
        frwrdbt1.getStyleClass().add("bt-st");
        frwrdbt1.setOnAction(new EventHandler<ActionEvent>() {
          @Override
          public void handle(ActionEvent event){
            app.navigateToPage2();
          }
        });

        Button frwrdbt2 = new Button("->");
        frwrdbt2.setMinHeight(5);
        frwrdbt2.setMinWidth(5);
        frwrdbt2.setLayoutX(780);
        frwrdbt2.setLayoutY(390);
        frwrdbt2.getStyleClass().add("bt-st");

        Button frwrdbt3 = new Button("->");
        frwrdbt3.setMinHeight(5);
        frwrdbt3.setMinWidth(5);
        frwrdbt3.setLayoutX(780);
        frwrdbt3.setLayoutY(580);
        frwrdbt3.getStyleClass().add("bt-st");

        Button frwrdbt4 = new Button("->");
        frwrdbt4.setMinHeight(5);
        frwrdbt4.setMinWidth(5);
        frwrdbt4.setLayoutX(780);
        frwrdbt4.setLayoutY(750);
        frwrdbt4.getStyleClass().add("bt-st");

        Image setting = new Image("profilebutton.jpg");

        ImageView settingIV = new ImageView(setting);
        settingIV.setFitWidth(50);
        settingIV.setFitHeight(50);
        settingIV.setPreserveRatio(false);

        Button seButton = new Button();
        seButton.setGraphic(settingIV);
        seButton.setMinHeight(0);
        seButton.setMinWidth(5);
        seButton.setLayoutX(1810);
        seButton.setLayoutY(17);
        seButton.getStyleClass().add("bt-st");
        seButton.setOnAction(new EventHandler<ActionEvent>() {
          @Override
          public void handle(ActionEvent event){
            app.navigateToPage6();
          }
        });

        Group gp = new Group(textStyle, root, root2, hb1, hb2, frwrdbt1, hb3, frwrdbt2, hb4, frwrdbt3, hb5, searchBox,

                frwrdbt4, seButton);
        view.add(gp,0,0);
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(3), e -> nextImage()));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    
  }

    public GridPane getView() {

        return view;
      }
      
    private Text createStyledText(String content, String fontFamily, FontWeight fontWeight, FontPosture fontPosture,
            double fontSize, double x, double y) {
        Text text = new Text(content);
        text.setFont(Font.font(fontFamily, fontWeight, fontPosture, fontSize));
        text.setFill(Color.BLACK);
        text.setX(50);
        text.setY(50);

        // DropShadow effect
        DropShadow dropShadow = new DropShadow();
        dropShadow.setOffsetX(3.0);
        dropShadow.setOffsetY(3.0);
        dropShadow.setColor(Color.color(0.4, 0.4, 0.4));

        // Glow effect
        Glow glow = new Glow();
        glow.setLevel(0.8);

        // Reflection effect
        Reflection reflection = new Reflection();
        reflection.setFraction(0.7);

        // Applying effects
        text.setEffect(dropShadow);
        dropShadow.setInput(glow);
        glow.setInput(reflection);

        return text;
    }

    private void updateImage() {
        Image image = new Image(imagePaths[currentIndex]);
        currentImageView.setImage(image);
        currentImageView.setFitWidth(800);
        currentImageView.setFitHeight(800);
        currentImageView.setPreserveRatio(true);
    }

    private void nextImage() {
        int nextIndex = (currentIndex + 1) % imagePaths.length;
        Image nextImage = new Image(imagePaths[nextIndex]);
        nextImageView.setImage(nextImage);
        nextImageView.setFitWidth(800);
        nextImageView.setFitHeight(800);
        nextImageView.setPreserveRatio(true);
        nextImageView.setVisible(true);

        TranslateTransition slideOut = new TranslateTransition(Duration.seconds(1), currentImageView);
        slideOut.setFromX(0);
        slideOut.setToX(-800);

        TranslateTransition slideIn = new TranslateTransition(Duration.seconds(1), nextImageView);
        slideIn.setFromX(800);
        slideIn.setToX(0);

        FadeTransition fadeOut = new FadeTransition(Duration.seconds(1), currentImageView);
        fadeOut.setFromValue(1.0);
        fadeOut.setToValue(0.9); // Partially fade out

        FadeTransition fadeIn = new FadeTransition(Duration.seconds(1), nextImageView);
        fadeIn.setFromValue(0.5); // Start from partially faded in
        fadeIn.setToValue(0.9);

        slideOut.setOnFinished(e -> {
            currentIndex = nextIndex;
            updateImage();
            nextImageView.setVisible(false);
            currentImageView.setTranslateX(0);
            nextImageView.setTranslateX(0);
            currentImageView.setOpacity(1.0);
            nextImageView.setOpacity(1.0);
        });

        ParallelTransition transitionOut = new ParallelTransition(slideOut, fadeOut);
        ParallelTransition transitionIn = new ParallelTransition(slideIn, fadeIn);

        transitionOut.play();
        transitionIn.play();
    }

    private void applyRoundedCorners(ImageView imageView, double radius) {
        Rectangle clip = new Rectangle(imageView.getFitWidth(), imageView.getFitHeight());
        clip.setArcWidth(50);
        clip.setArcHeight(50);
        imageView.setClip(clip);

        // Update the clip when the image view size changes
        imageView.fitWidthProperty().addListener((obs, oldVal, newVal) -> {
            clip.setWidth(newVal.doubleValue());
        });

        imageView.fitHeightProperty().addListener((obs, oldVal, newVal) -> {
            clip.setHeight(newVal.doubleValue());
        });
    }
    
    }
