package com.source.Data;

import com.source.controller.FromNavigationApp;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Glow;
import javafx.scene.effect.Reflection;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

public class LandingPage {

  private FromNavigationApp app;
  private GridPane view;
  private TextField field1;

  public LandingPage(FromNavigationApp app) {

    this.app = app;
    initialize();
  }

  private void initialize() {

    view = new GridPane();
    view.setPadding(new Insets(1,1,1,1));
    view.setStyle("-fx-background-Color:LIGHTGREEN");
      // Creating buttons
                //Button bt1 = new Button("Courses");
                Button bt2 = new Button("Forum");
                Button bt3 = new Button("Contact");
                bt3.setOnAction(new EventHandler<ActionEvent>() {
                  
                  @Override
                  public void handle(ActionEvent event){
                    app.navigateToPage8();
                  }
                });

                Button bt4 = new Button("About Us");
                bt4.setOnAction(new EventHandler<ActionEvent>() {
                  
                  @Override
                  public void handle(ActionEvent event){
                    app.navigateToPage7();
                  }
                });

                Button bt5 = new Button("Register");
                bt5.setOnAction(new EventHandler<ActionEvent>() {
                  
                  @Override
                  public void handle(ActionEvent event){
                    app.navigateToPage5();
                  }
                });

                // Styling the buttons to show only text with a good font
                String buttonStyle = "-fx-background-color: transparent; -fx-text-fill: white; -fx-font-size: 16pt; -fx-font-family: 'Arial'; -fx-font-weight: bold;";
               // bt1.setStyle(buttonStyle);
                bt2.setStyle(buttonStyle);
                bt3.setStyle(buttonStyle);
                bt4.setStyle(buttonStyle);

                Text text2 = createStyledText("Quizzo!", "Arial", FontWeight.BOLD, FontPosture.ITALIC, 50, 50, 60);

                HBox hb1 = new HBox(text2); // Adding spacing between buttons
                hb1.setPadding(new Insets(20)); // Adding padding around the HBox
                hb1.setMinWidth(1920);
                hb1.setMinHeight(100);
                hb1.setLayoutX(0);
                hb1.setLayoutY(0);
                hb1.setStyle("-fx-background-color: BLACK;" + "-fx-background-square: 30;");

                // Create a spacer to push the buttons to the right
                Region spacer = new Region();
                HBox.setHgrow(spacer, Priority.ALWAYS);
                hb1.getChildren().addAll(spacer,  bt2, bt3, bt4, bt5);

                Image landImg = new Image("img1.png");

                ImageView landIv = new ImageView(landImg);
                landIv.setFitHeight(500);
                landIv.setFitWidth(500);
                landIv.setLayoutX(1420);
                landIv.setLayoutY(120);

                Text welcomeText = new Text("Welcome to the Ultimate Quiz App!");
                welcomeText.setFont(Font.font("Arial", FontWeight.EXTRA_BOLD, 50));
                welcomeText.setFill(Color.BLACK);
                welcomeText.setLayoutX(170);
                welcomeText.setLayoutY(180);

                Text studyText = new Text("“Enhance your knowledge, challenge your mind, and have fun learning!”");
                studyText.setFont(Font.font("Arial", FontWeight.NORMAL, 40));
                studyText.setFill(Color.DARKBLUE);
                studyText.setLayoutX(120);
                studyText.setLayoutY(250);

                Text motivationText = new Text(
                                "“The beautiful thing about learning is that no one can take it away from you.”");
                motivationText.setFont(Font.font("Arial", FontWeight.EXTRA_BOLD, 40));
                motivationText.setFill(Color.BLACK);
                motivationText.setLayoutX(25);
                motivationText.setLayoutY(900);

                Image cmpImg = new Image("cmp1.png");

                ImageView cmpIv = new ImageView(cmpImg);
                cmpIv.setFitHeight(300);
                cmpIv.setFitWidth(300);
                cmpIv.setStyle("-fx-background-radius: 30;");

                VBox vb1 = new VBox(cmpIv); // Adding spacing between elements
                vb1.setMinWidth(300);
                vb1.setMinHeight(365);
                vb1.setLayoutX(40);
                vb1.setTranslateY(380);
                vb1.setStyle("-fx-background-color: BLACK;" + "-fx-background-radius: 30;");
                vb1.setMargin(cmpIv, new Insets(30, 10, 10, 10));

                Image cmpImg1 = new Image("cmp2.png");

                ImageView cmpIv2 = new ImageView(cmpImg1);
                cmpIv2.setFitHeight(300);
                cmpIv2.setFitWidth(300);
                cmpIv2.setStyle("-fx-background-radius: 30;");

                VBox vb2 = new VBox(cmpIv2); // Adding spacing between elements
                vb2.setMinWidth(300);
                vb2.setMinHeight(365);
                vb2.setLayoutX(400);
                vb2.setTranslateY(380);
                vb2.setStyle("-fx-background-color: BLACK;" + "-fx-background-radius: 30;");
                vb2.setMargin(cmpIv2, new Insets(30, 10, 10, 10));

                Image phyImg1 = new Image("phy1.png");

                ImageView phyIv1 = new ImageView(phyImg1);
                phyIv1.setFitHeight(300);
                phyIv1.setFitWidth(300);
                phyIv1.setStyle("-fx-background-radius: 30;");

                VBox vb3 = new VBox(phyIv1); // Adding spacing between elements
                vb3.setMinWidth(300);
                vb3.setMinHeight(365);
                vb3.setLayoutX(760);
                vb3.setTranslateY(380);
                vb3.setStyle("-fx-background-color: BLACK;" + "-fx-background-radius: 30;");
                vb3.setMargin(phyIv1, new Insets(30, 10, 10, 10));

                Image chemImg1 = new Image("chem.png");

                ImageView chemIv1 = new ImageView(chemImg1);
                chemIv1.setFitHeight(320);
                chemIv1.setFitWidth(300);
                chemIv1.setStyle("-fx-background-radius: 30;");

                VBox vb4 = new VBox(chemIv1); // Adding spacing between elements
                vb4.setMinWidth(300);
                vb4.setMinHeight(365);
                vb4.setLayoutX(1100);
                vb4.setTranslateY(380);
                vb4.setStyle("-fx-background-color: BLACK;" + "-fx-background-radius: 30;");
                vb4.setMargin(cmpIv2, new Insets(30, 10, 10, 10));

                HBox hb2 = new HBox(); // Adding spacing between elements
                hb2.setMinWidth(1500);
                hb2.setMinHeight(900);
                hb2.setStyle("-fx-background-color: GREEN;" + "-fx-background-square: 30;");
                hb2.setLayoutX(00);
                hb2.setLayoutY(350);
                hb2.setMargin(cmpIv, new Insets(25, 10, 10, 10));

                Group gp = new Group(welcomeText, studyText, hb1, hb2, motivationText, vb1, vb2, vb3, vb4,
                                landIv);

      view.add(gp, 0, 0);

  }

  private Text createStyledText(String content, String fontFamily, FontWeight fontWeight, FontPosture fontPosture,
  double fontSize, double x, double y) {
Text text = new Text(content);
text.setFont(Font.font(fontFamily, fontWeight, fontPosture, fontSize));
text.setFill(Color.GREEN);
text.setX(100);
text.setY(950);

// DropShadow effect
DropShadow dropShadow = new DropShadow();
dropShadow.setOffsetX(3.0);
dropShadow.setOffsetY(3.0);
dropShadow.setColor(Color.color(0.4, 0.4, 0.4));

// Glow effect
Glow glow = new Glow();
glow.setLevel(0.8);

// Reflection effect
Reflection reflection = new Reflection();
reflection.setFraction(0.7);

// Applying effects
text.setEffect(dropShadow);
dropShadow.setInput(glow);
glow.setInput(reflection);

return text;
  }
  public GridPane getView() {

    return view;
  }

  // public String getField1Value() {

  //   return field1.getText();
  // }

  // public void setField1Value(String value) {

  //   field1.setText(value);
  // }

}
