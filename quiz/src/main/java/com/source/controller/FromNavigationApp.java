package com.source.controller;

import com.source.Data.JavaQuizExample;

import com.source.Data.LandingPage;
import com.source.Data.Nextpage;
import com.source.Data.aboutUs;
import com.source.Data.contact;
import com.source.Data.homePage;
import com.source.Data.homePage2;
import com.source.Data.javaQuiz;

import com.source.Data.loginController;
import com.source.Data.register;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class FromNavigationApp extends Application{


    private Stage primaryStage;
    private Scene page1Scene, page2Scene , page3Scene , page4Scene, page5Scene, page6Scene, page7Scene, page8Scene, page9Scene;
    
    
    private LandingPage page1;

    private homePage page2;

    private homePage2 page3;

    private javaQuiz page4;

    private register page5;

    private Nextpage page6;

    private aboutUs page7;

    private contact page8;

    private JavaQuizExample page9;



    @Override
    public void start(Stage primaryStage){
        this.primaryStage = primaryStage;

        page1 = new LandingPage(this);
        page2 = new homePage(this);
        page3 = new homePage2(this);
        page4 = new javaQuiz(this);
        page5 = new register(this);
        page6 = new Nextpage(this);
        page7 = new aboutUs(this);
        page8 = new contact(this);
        page9 = new JavaQuizExample(this);


        page1Scene = new Scene(page1.getView(),1920,990);
        page1Scene.getStylesheets().add(getClass().getResource("landing.css").toExternalForm());

        page2Scene = new Scene(page2.getView(),1920,990);
        page2Scene.getStylesheets().add(getClass().getResource("style2.css").toExternalForm()); 

        page3Scene = new Scene(page3.getView(),1920,990);
        page3Scene.getStylesheets().add(getClass().getResource("landing.css").toExternalForm());

        page4Scene = new Scene(page4.getView(),1920,990);
        page4Scene.getStylesheets().add(getClass().getResource("style3.css").toExternalForm());

        page5Scene = new Scene(page5.getView(),1920,990);
      
         page6Scene = new Scene(page6.getView(),1920,990);
         page6Scene.getStylesheets().add(getClass().getResource("landing.css").toExternalForm());

         page7Scene = new Scene(page7.getView(),1920,990);
         page7Scene.getStylesheets().add(getClass().getResource("about.css").toExternalForm()); 

        page8Scene = new Scene(page8.getView(),1920,990);
        page8Scene.setFill(Color.LIGHTGREEN);

        page9Scene = new Scene(page9.getView(),1920,990);

        primaryStage.setScene(page1Scene);
        primaryStage.setTitle("From Navigation");
        primaryStage.show();

    }


    public void navigateToPage1(){

        primaryStage.setScene(page1Scene);

    }
   
     public void navigateToPage2(){

         primaryStage.setScene(page2Scene);

    }
   
    public void navigateToPage3(){

        primaryStage.setScene(page3Scene);

    }
    public void navigateToPage4(){

        primaryStage.setScene(page4Scene);

    }
    public void navigateToPage5(){

        primaryStage.setScene(page5Scene);

    }
    public void navigateToPage6(){

     
        primaryStage.setScene(page6Scene);

    }
    public void navigateToPage7(){

  
        primaryStage.setScene(page7Scene);

    }

    public void navigateToPage8(){

  
        primaryStage.setScene(page8Scene);

    }
    public void navigateToPage9(){

  
        primaryStage.setScene(page9Scene);

    }
    
    
}