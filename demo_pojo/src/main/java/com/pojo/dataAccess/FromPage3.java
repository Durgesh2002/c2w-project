package com.pojo.dataAccess;


import com.pojo.controller.FromNavigationApp;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class FromPage3 {

    private FromNavigationApp app;
    private GridPane view;
    private TextField field3;

    public FromPage3 (FromNavigationApp app){
        this.app = app;
        initialize();

    }
    private void initialize(){

        view = new GridPane();
        view.setPadding(new Insets(10));
        try{
             view.setHgap(13);
            view.setVgap(13);
        }
        catch(Exception e){

            System.out.println("Error hai Don");
        }
        Label label3 = new Label("Field 3");
        field3 = new TextField();

        Button backButton = new Button("Next");

        backButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event){

                app.navigateToPage2();
            }
        });

        view.add(label3 ,0,0);
        view.add( field3,1,0);
        view.add(backButton ,0,1);
    
    }

    public GridPane getView(){

        return view;
    }

    public String getField3Value(){

        return field3.getText();
    }

    public void setField3Value(String value){

        field3.setText(value);
    }

}
