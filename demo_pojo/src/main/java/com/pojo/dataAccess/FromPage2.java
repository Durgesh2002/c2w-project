package com.pojo.dataAccess;


import javax.swing.Action;

import com.pojo.controller.FromNavigationApp;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.chart.BubbleChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class FromPage2 {

    private FromNavigationApp app;
    private GridPane view;
    private TextField field2;

    public FromPage2 (FromNavigationApp app){
        this.app = app;
        initialize();

    }
    private void initialize(){

        view = new GridPane();
        view.setPadding(new Insets(10));

        view.setHgap(10);
        view.setVgap(10);

        Label label2 = new Label("Field 2");
        field2 = new TextField();

        Button nextButton = new Button("Next");

        nextButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event){

                app.navigateToPage3();
            }
        });

        Button backButton = new Button("Back");

        backButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event){

                app.navigateToPage1();
            }
        });

        Label lb=new Label("CPP");
        lb.setFont(new Font(25));
        lb.setTextFill(javafx.scene.paint.Color.RED);
        VBox vi1=new VBox(lb);
    
        vi1.setAlignment(Pos.CENTER);
        vi1.setMinSize(350, 150);
        vi1.setLayoutX(300);
        vi1.setLayoutY(200);                      
        vi1.setStyle("-fx-background-color:BLACK;");
        vi1.setLayoutX(300);
        vi1.setLayoutY(200);                      
        vi1.setStyle("-fx-background-color:BLACK;");

        Label lb2=new Label("PYTHON");
        lb2.setFont(new Font(25));
        VBox vi2=new VBox(lb2);
        vi2.setAlignment(Pos.CENTER);
        vi2.setMinSize(350, 150); 
        vi2.setLayoutX(300);
        vi2.setLayoutY(350);                     
        vi2.setStyle("-fx-background-color:YELLOW;");
       
        Label lb3=new Label("NODEJS");
        lb3.setFont(new Font(25));
        VBox vi3=new VBox(lb3);
        vi3.setAlignment(Pos.CENTER);
        vi3.setMinSize(350, 150); 
        vi3.setLayoutX(300);
        vi3.setLayoutY(500);                     
        vi3.setStyle("-fx-background-color:PINK;");

        Label lb4=new Label("FLUTTER");
        lb4.setFont(new Font(25));
        VBox vi4=new VBox(lb4);
        vi4.setAlignment(Pos.CENTER);
        vi4.setMinSize(350, 150);
        vi4.setLayoutX(900);
        vi4.setLayoutY(50);                      
        vi4.setStyle("-fx-background-color:PURPLE;");

        Label lb5=new Label("JS");
        lb5.setFont(new Font(25));
        VBox vi5=new VBox(lb5);
        vi5.setAlignment(Pos.CENTER);
        vi5.setMinSize(350, 150);
        vi5.setLayoutX(900);
        vi5.setLayoutY(200);                      
        vi5.setStyle("-fx-background-color:BLUE;");

        Label lb6=new Label("REACT");
        lb6.setFont(new Font(25));
        VBox vi6=new VBox(lb6);
        vi6.setAlignment(Pos.CENTER);
        vi6.setMinSize(350, 150); 
        vi6.setLayoutX(900);
        vi6.setLayoutY(350);                     
        vi6.setStyle("-fx-background-color:GREEN;");
        
        Label lb7=new Label("SPRING");
        lb7.setFont(new Font(25));
        VBox vi7=new VBox(lb7);
        vi7.setAlignment(Pos.CENTER);
        vi7.setMinSize(350, 150); 
        vi7.setLayoutX(900);
        vi7.setLayoutY(500);                     
        vi7.setStyle("-fx-background-color:ORANGE;");

        // view.add(label2 ,0,0);
        // view.add( field2,1,0);
        // view.add(backButton ,0,1);
        // view.add(nextButton ,1,1);
        Group gp = new Group(vi1,vi2,vi3,vi4,vi5,vi6,vi7,nextButton,backButton,label2,field2);
        view.add(gp,0,0);
    
    }

    public GridPane getView(){

        return view;
    }

    public String getField2Value(){

        return field2.getText();
    }

    public void setField2Value(String value){

        field2.setText(value);
    }

}
