package com.pojo.dataAccess;



import javax.swing.Action;

import com.pojo.controller.FromNavigationApp;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.chart.BubbleChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class FromPage1 {

    private FromNavigationApp app;
    private GridPane view;
    private TextField field1;


    public FromPage1 (FromNavigationApp app){
        this.app = app;
        initialize();

    }
     private void initialize(){

        view = new GridPane();
        view.setPadding(new Insets(10));

        view.setHgap(10);
        view.setVgap(10);

        Label label1 = new Label("Field 1");
        field1 = new TextField();

        Button nextButton = new Button("Next");

        nextButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event){

                app.navigateToPage2();
            }
        });

   

        //Label label = new Label("Hello");

        Text text2 = new Text("Quizzo!");
        //text2.setFont(new Font(170)); // Set the font size
       // text2.getStyleClass().add("text-style"); // Set the tex
        // Position the text at the desired location
        text2.setX(150); // X position
        text2.setY(60); // Y position   


        // Button button1 = new Button("<- Back");
        // button1.setLayoutX(100);
        // button1.setLayoutY(100);
        // button1.setMinWidth(30);
        // button1.setMinHeight(10);
        // button1.setFont(new Font(25));



        Group gp = new Group(text2,nextButton);

        // view.add(gp,0,0);
        // view.add(label1 ,1,2);
        // view.add(field1 ,1,0);
        view.add(gp ,0,0);

    }

    public GridPane getView(){

        return view;
    }

    public String getField1Value(){

        return field1.getText();
    }

    public void setField1Value(String value){

        field1.setText(value);
    }

}
