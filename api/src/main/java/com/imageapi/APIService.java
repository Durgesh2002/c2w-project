package com.imageapi;

import org.json.JSONObject;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class APIService extends Application {

    static String imgURL = "";

    public static void imageData()throws Exception {

        StringBuffer response = new DataUrls().getResponseData();
        if(response != null){

            JSONObject obj = new JSONObject(response.toString());
            JSONObject urlObject = obj.getJSONObject("urls");
            imgURL = urlObject.getString("small");
    
        }
        else{
            System.out.println("Response is empty");
        }

    }

    @Override
    public void start(Stage primaryStage) throws Exception {

            imageData();

            Image image = new Image(imgURL);

            ImageView imageView = new ImageView(image);

            Pane imgPane = new Pane();
            imgPane.getChildren().add(imageView);

            GridPane GP = new GridPane();
            GP.setPadding(new Insets(10,10,10,10));
            GP.setVgap(8);
            GP.setHgap(10);

            Label lb = new Label("User Name");
            GP.setConstraints(lb, 0, 0);
            

            Group gp = new Group(GP);
            Scene scene = new Scene(imgPane, image.getWidth(),image.getHeight());
            primaryStage.setScene(scene);

            primaryStage.setTitle("ImageView Example");

            primaryStage.show();

        }
    }

    
